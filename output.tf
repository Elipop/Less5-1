output "ip_addr" {
  value = "${google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip}"
  description = "nat_ip"
}
output "name" {
  value = "${google_compute_instance.vm_instance.*.name}"
  description = "Hostname"
}
