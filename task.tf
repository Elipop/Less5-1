variable private_key_path {
  description = "priv key"
}
variable public_key_path {
  description = "public key"
}

variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "vm_instance" {
  count        = "${var.node_count}"
  name         = "node${count.index + 1}"
  machine_type = "e2-small"

provisioner "local-exec" {
  command = "echo ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
}
provisioner "local-exec" {
  command = "echo ${self.name} >> host.list"
}
#Прокинем ключ ssh_keys
metadata = {
    ssh-keys = "root:${file(var.public_key_path)}"
  }

provisioner "remote-exec" {
# script = "scripts/install.sh"

connection {
    type = "ssh"
    user = "root"
    private_key = "${file(var.private_key_path)}"
#    host_key = "${file(var.public_key_path)}"
    timeout = "2m"
    agent = "true"
    host = "${self.network_interface.0.access_config.0.nat_ip}"
  }
 inline = [
  "sudo apt-get update",
  "sudo apt-get -y install nginx",
  "echo Juneway > /var/www/html/index.nginx-debian.html",
  "hostname -i >> /var/www/html/index.nginx-debian.html",
  "hostname >> /var/www/html/index.nginx-debian.html"
]
}


boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = "20"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
}
